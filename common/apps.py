"""
Release: 0.2.2
Author: Golikov Ivan
Date: 10.07.2017
"""

from django.apps import AppConfig


class CommonConfig(AppConfig):
    name = 'common'
