"""
Release: 0.2.2
Author: Golikov Ivan
Date: 10.07.2017
"""

from django.contrib import admin

admin.site.site_header = 'Кроссовые журналы'
admin.site.site_title = 'Кроссовые журналы'
